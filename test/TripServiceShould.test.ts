// tslint:disable: max-classes-per-file
import "jest";
import TripService from "../src/trip/TripService";
import User from "../src/user/User";
import UserNotLoggedInException from "../src/exception/UserNotLoggedInException";
import Trip from "../src/trip/Trip";
import TripDAO from "../src/trip/TripDAO";
import UserSessionService from "../src/user/UserSessionService";
import UserService from "../src/user/UserService";
import TripDAOStub from "./stubs/TripDAOStub";
import UserSessionServiceStub from "./stubs/UserSessionServiceStub";

describe("TripServiceShould", () => {

    it("throw error if the user is not logged", () => {
        const tripService = new TripService(
            new TripDAOStub(), 
            new UserSessionServiceStub(),
            new UserService()
        );
        const anyUser = new User();
        
        const getTripsByUser = () => tripService.getTripsByUser(anyUser);

        expect(getTripsByUser).toThrowError(UserNotLoggedInException);
    });

    it("get the trips of a friend", () => {
        const myFriend = new User();
        const myUser = new User();
        const tripService = new TripService(
            new TripDAOStub(), 
            new UserSessionServiceStub(myUser), 
            new UserService()
        );
        
        myFriend.addFriend(myUser);

        const trips = tripService.getTripsByUser(myFriend);

        expect(trips).toBe(myFriend.getTrips());
    });

    
    it("get a empty list of trips when the queried user is not friend of the logged user" 
     + " AND the queried user has friends", () => {
        const notMyFriend = new User();
        const myUser = new User();
        const otherUser = new User();
        const tripService = new TripService(
            new TripDAOStub(), 
            new UserSessionServiceStub(myUser), 
            new UserService()
        );
        
        notMyFriend.addFriend(otherUser);

        const trips = tripService.getTripsByUser(notMyFriend);

        expect(trips).toEqual([]);
    });

    it("get a empty list of trips when the queried user doesn't have any friend", () => {
        const notMyFriend = new User();
        const myUser = new User();
        const tripService = new TripService(
            new TripDAOStub(), 
            new UserSessionServiceStub(myUser), 
            new UserService()
        );
        
        const trips = tripService.getTripsByUser(notMyFriend);

        expect(trips).toEqual([]);
    });
    
});
