import TripDAO from "../../src/trip/TripDAO";
import User from "../../src/user/User";

export default class TripDAOStub extends TripDAO {
    public findTripsByUser(user: User) {
        return user.getTrips();
    }
}