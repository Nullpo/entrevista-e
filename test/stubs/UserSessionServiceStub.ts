import User from "../../src/user/User";
import UserSessionService from "../../src/user/UserSessionService";

export default class UserSessionServiceStub extends UserSessionService {
    constructor(private user?: User) { 
        super();
    }

    public getLoggedUser(): User {
        return this.user;
    }
}