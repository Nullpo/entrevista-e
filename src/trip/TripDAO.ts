import CollaboratorCallException from "../exception/CollaboratorCallException";
import User from "../user/User";
import Trip from "./Trip";

export default class TripDAO {
    public static getInstance(): TripDAO {
        return TripDAO.instance;
    }

    private static readonly instance = new TripDAO();

    public findTripsByUser(user: User): Trip[] {
        throw new CollaboratorCallException(
            "TripDAO should not be invoked on an unit test.");
    }
}
