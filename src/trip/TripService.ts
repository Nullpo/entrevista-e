import UserNotLoggedInException from "../exception/UserNotLoggedInException";
import User from "../user/User";
import UserService from "../user/UserService";
import UserSessionService from "../user/UserSessionService";
import Trip from "./Trip";
import TripDAO from "./TripDAO";

export default class TripService {

  constructor(
    private tripDao: TripDAO,
    private userSessionService: UserSessionService,
    private userService: UserService,
    ) { }

  public getTripsByUser(user: User): Trip[] {
    const loggedUser: User = this.userSessionService.getLoggedUser();

    if (loggedUser) {
      if (this.userService.isUserFriendOf(loggedUser, user)) {
        return this.tripDao.findTripsByUser(user);
      }
      return [];
    } else {
      throw new UserNotLoggedInException();
    }
  }
}
