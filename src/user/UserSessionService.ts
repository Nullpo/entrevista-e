import CollaboratorCallException from "../exception/CollaboratorCallException";
import User from "./User";

export default class UserSessionService {
    public getLoggedUser(): User {
        throw new CollaboratorCallException(
            "UserSession.getLoggedUser() should not be called in an unit test",
        );
    }
}
