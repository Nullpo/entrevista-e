import User from "./User";

export default class UserService {
    public isUserFriendOf(myself: User, user: User): boolean {
        return user.getFriends().some((e: User) => e === myself);
    }
}
