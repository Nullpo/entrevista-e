Decisiones de diseño:

Nota: En master está la versión que, basado en lo que hablamos, pienso que es la solución a la que quieren llegar.
En el branch "refactor/user-session" hay una solución alternativa con la que me siento más a gusto.

Decisiones de diseño:

Primera iteracion 
-----------------

Objetivo: Eliminar acoplamiento de TripDAO en TripService.

1 - Si bien se mencionó que TripDAO es un singleton, de hecho no lo es, si no que es una clase utilitaria.
Para convertirlo en un Singleton, cambié el método findTripsByUser a público, y le agregué un static getInstance. La instancia es creada cuando se crea la clase, para poder usar la keyword readonly, pero podría ser "lazy". 

2 - De esta forma, puedo agregar un stub que herede de TripDAO, y pasarselo por constructora TripService. Luego, se hace inutil tener TripService#findTripsByUser (lo cual, como mencioné en la entrevista, me "huele mal" ya que estoy modificando la clase que estoy testeando, lo cual es suceptible a errores).

En pos de economizar, el stub lo hago extendiendo de la clase TripDAO. Pero dependiendo del proyecto, otra opcion sería que TripDAO sea una interfaz, y tener dos implementaciones distintas: TripDAOImpl, que es actual, el que tira la exception, y TripDaoStub, así mantengo separada una implementación de la otra (o tal vez hacer una factory dependiendo del NODE_ENV). No lo hice debido a que me pareció que generaba un código más complejo innecesariamente. 

Segunda iteración
-----------------


Objetivo: Eliminar acoplamiento de UserSession en TripService.

3 - UserSession es un caso similar, solo que en el archivo se expone una instancia de UserSession y no la clase. Asi que preferí convertirlo en un servicio ya que, al exponer una instancia, no me queda claro como getLoggedUser obtiene "en la vida real" el mismo. Por ahora asumo que lo obtiene del contexto del stack de alguna forma (usando request-context o continuation-local).

Tercer iteración
----------------

Objetivo: Reducir responsabilidad extra de TripService (obtener "amigos" de un usuario).

Aqui me presento ante un problema que comenté en la entrevista:

  - Desde un punto de vista de responsabilidades entre objetos, es claro que el saber si una persona es amiga de otra es responsabilidad de User ya que es parte de mi dominio. No colocar dicha logica en ese objeto, hace que User termine siendo un objeto anemico, ya que solamente se encarga de almacenar datos. Esto requeriría simplemente enviar el método privado TripService#isMyFriend como público en la clase User, y el parametro "myself" en realidad sería "this".

  - Desde un punto de vista de performance, sería más lógico que sea un servicio/repositorio que se encargue de obtener dicha data desde una fuente (ej: una base de datos/un endpoint externo). En ese caso, debería de pensar en porqué tengo el objeto User en memoria, y no, por ejemplo, un ID.

Debido a que en la entrevista quedamos en que sería mejor hacer un servicio, asumo que friends se llena de forma lazy / el User#getFriends levanta los amigos "a demanda" y no los tiene en memoria en ese momento. Asi que se creó UserService#isUserFriendOf, el cual actualmente tiene la logica de TripService#isMyFriend.

Cuarta iteración
----------------

Objetivo: Mejorar el código en general.

- Los stubs se pasaron a otra carpeta.
- Se agrego como opcional el parametro "user" en UserSessionServiceStub para eliminar un null.
- Se cambió el package.json para que corra solo los archivos que terminen con ".test.ts".

Quinta iteración (extra)
------------------------

Esto se puede ver en el branch refactor/user-session. 

Esta la hago debido a que cuando consulté si podía modificar la interfaz de TripService los noté sorprendidos, pero me dieron el OK para crear un constructor desde el cual se inyecten las dependencias, así que asumí que no era la solucion que querían que haya. 

Para mi, TripService#getTripsByUser debería de recibir el usuario logueado, ademas del usuario original, ya que esta clase estaría entendiendo que hay un usuario logueado, lo cual para mi debería ser responsabilidad de una capa externa, y que TripService reciba solamente dos usuarios. De esta forma, esta clase solo tiene una responsabilidad y su scope se acota a logica de negocio.

Para el ejercicio, ya no tendría sentido la existencia de UserSessionService, ya que el usuario logueado es enviado por el consumidor de TripService. Al igual que ya no hablamos de "usuario logueado" si no de "usuario que está accediendo" (no se me ocurrió mejor nombre).

Por ultimo, si se hubiese elegido en la "Tercer iteración" la opcion de que el usuario sea el responsable de conocer a sus amigos y no un servicio (asumiendo que es barato obtener el listado de amigos), esa logica podría pasar directamente a User#getTripsOfMyFriend(user: User), y de esta forma TripService ya no tendría sentido: Toda la logica de un usuario estaría en User, con lo cual el codigo de todo el proyecto se reduciría a agregarle un metodo a User:

```
public getTripsOfMyFriend(user) { 
   if(!user) throw InvalidUserException();
   return user.getFriends().some((e: User) => e === this);
}
```

Reduciendo este problema a su mas minima expresión. Luego, existiría un servicio que obtenga el usuario logueado o returne null, y un controller/middleware que se encargue de tirar un UserNotLoggedException si no está loggeado, pero para realizar esta solución necesitaría más contexto (ya que solamente existirían cuatro archivos: User, Trip, LoggedUserService, y algun repositorio que obtenga usuarios). Me imagino que la idea del ejercicio no es de ninguna manera llega a este nivel, si no mostrar división por capas, pero no podía dejar de comentarlo.


Comentario adicional:
---------------------

Se que esto es completamente fuera de scope, pero al ver que ningun objeto altera dinamicamente su estado, no encuentro motivo alguno por el cual usar Clases y no directamente funciones en donde, ayudados por el scope, primero se inyecten las dependencias y luego se ejecute la funcion. Por ej:

```
export const getTripFromUserBuilder = (dao: TripDAO) => (user: User, accessor: Accessor) =>{
  if(user.getFriends(...).some(...)) return dao.getTrip(...)
}

```

y luego de inyectar los servicios, se podria hacer:

```
const getTripFromUser = getTripFromUserBuilder(tripDAO);

... en una request ...

Try(loggedUser)
  .map(extractQueryUser)
  .map(getTripFromUser)
  .mapWith({
    ok: ...conversor a capa de comunicacion, por ej, 200 Ok,
    error: ...conversor de excepción de negocio a, por ej, http 400, 404, etc
  })
```

Esta solución requeriría crear/importar la monada Try/Either, aunque tambien podría hacerse simplemente con composición de funciones.

Perdon por haber sido extenso, pero siendo que me lo dieron para que lo siga desde mi casa, preferí agregar detalle que dejar dudas sobre mi tren de pensamiento.